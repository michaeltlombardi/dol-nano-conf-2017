# DevOps Library Nano Conference April 2017 Trip Report

* Bellevue, WA
* April 13, 2017
* Mike Lombardi

## What is DevOps Library Nano Conference

This small conference was a rare chance to spend the day with expert practitioners of DevOps principles _primarily_, but not exclusively, in the Windows stack.
The focus for this small conference was around a few interesting talks and the open spaces discussions.

## Why I chose to attend DevOps Library Nano Conference

I wanted to attend this conference primarily to have access to the minds of the other attendees for the day.
I anticipated that the discussions I would have with these folks would help drive me towards better practices.

I was most looking forward to the open spaces discussions and to getting feedback on patterns and techniques used by other practitioners.

## Takeaways

+ We need to be solving for application configuration and automation
  + We need to do so in the "brown field" too, because green field isn't a real option.
+ We really need to focus on building a repeatable pipeline and making our deployments and changes safer and saner.

## Detailed Session Information

### Infrastructure and Application Automation and the Future of DevOps with Steven Murawski

#### Summary
A talk about how we currently execute application configuration/automation, how we _should_ be doing it, and some patterns for this as well as a tool recommendation ([Habitat](https://habitat.sh))

#### Takeaways
+ The point of infrastructure automation is safe, repeatable, on-demand changes
  + Speed is a second order benefit
+ Common sense != Common practice
+ We can make environment changes into _standard changes_ by establishing positive control of our environment and change process
+ Appliances don't have cross machine dependencies, applications do
+ Most people are deploying ~5 containers per host
  + They're using containers more like lightweight VMs for packaging/management
  + Possibly because application configuration is easier this way than with "traditiona;" config management tools
+ We NEED to know our dependencies and explicitly enforce those.
+ To make management of our fleets easier/saner, we need to know our dependencies, use a standardized management API, understand our application lifecycle, and solve both secrets management and service discovery.
+ Habitat (or a tool _like it_) is probably what we should be looking at to solve application configuration and use chef/puppet/whatever for the underlying infrastructure.

#### Action Items
+ [x] Read the Habitat docs
+ [ ] Package a single existing application via habitat myself
+ [ ] Work with my team to develop a sane tooling around the pipeline - potentially using GitLab CI.
+ [ ] Discuss the sane/safe deployment pattern with my team members and org

### [DevOps, Desktop, and Odd Socks](https://speakerdeck.com/glennsarti/desktops-devops-and-odd-socks-chapter-2) with Glenn Sarti

#### Summary
This talk provided a lot of value by including some really useful real-world patterns and advice around DevOps adoption, security, and allies.
It also included numerous links to further materials.
#### Takeaways
+ Learn to speak the language of audit so you can convert auditors into allies
+ There's a huge overlap of ITIL and DevOps best practices.
+ Desktop Support / Service Desk are an amazing resource for evangelism and allyship
+ We should be able to perform continuous delivery of desktops like applications
+ Be mindful of access - don't share _any_ accounts, maintain least privilege, guard secrets and pipeline
+ Focus on an appropriate approval process (rubber stamping isn't useful, bikeshedding isn't either)
  + Define risks for your changes, define the criteria for each change type, get peer review
+ Be sensible about data security and tie it back to risks and risk management
+ Turn on your auditing tools and protect your logs
+ Measure things that matter
  + But be careful, because measuring things can steer you way off course
+ Know your business and risks to it, not just your particular slice
+ Seek auditor feedback!
+ **ACT** on the information you learn
+ Seek allies across your organization and local communities
  + Marketing, finance, business processes, sales, other IT departments - all of them are valuable sources of help

#### Action Items
+ [ ] Read [Effective DevOps: Building a Culture of Collaboration, Affinity, and Tooling at Scale](https://www.amazon.com/dp/1491926309/ref=cm_sw_r_cp_ep_dp_-377ybRP2JKRG)
+ [x] Read [Lean Enterprise: How High Performance Organizations Innovate at Scale](https://www.amazon.com/Lean-Enterprise-Performance-Organizations-Innovate/dp/1449368425)
+ [ ] Look into [sikuli](http://www.sikuli.org/) for desktop testing for our allies on that side of the house
+ [ ] Watch Joanne Molesky's [talk on Compliance + DevOps](https://www.youtube.com/watch?v=UKea1NisG2g)
+ [ ] Go through Jason Gray's [talk  on ITIL + DevOps](http://c.ymcdn.com/sites/www.itsmf.org.au/resource/resmgr/Docs/503_gray_jason_140813.pdf)
+ [x] Read [The Five Dysfunctions of a Team](https://www.amazon.com/Five-Dysfunctions-Team-Leadership-Fable/dp/0787960756)
+ [ ] Reach out to service desk folks in my org
+ [ ] Reach out to compliance team for lunch and chat
+ [x] Lurk in compliance channels in different slacks / seek out that community to learn from them.
+ [ ] Go get a better grip on my orgs risks and opportunities and keep those in focus for myself.

### Invoke-ChatOps: ChatOps for PowerShell Users with Brandon Olin

#### Summary
This talk was both around ChatOps generally and specifically around using PoshBot in the later sections.

#### Takeaways
+ The primary value of ChatOps is to _keep work in context and make it available to everyone in your team(s)_
+ Additional social benefits include knowledge sharing, team empowerment, improved situational awareness, faster onboarding, history for when people are AFK for more than a few minutes/hours, and evidence/history for use in retrospectives.
+ Technical benefits include an easier and more visible vector for launching automated tasks, reducing MTTR by making context available and shortening sync times, selective attention raising _in_ context, better history and logging of actions taken, improved safety.

#### Action Items
+ [x] Read [Atlassian's "What is ChatOps" Adoption Guide](https://blog.hipchat.com/2016/02/05/chatops-guide-evolution-adoption-significance/)
+ [x] Experiment with PoshBot
+ [x] Pester Brandon about concept guides for PoshBot architecture ~~/ help write them~~
+ [ ] Work with the PoshBot project to offer deployment of the bot as a service on a node.
+ [ ] Evangelize principles of ChatOps, lead by example

### Open Spaces

#### Summary
The conference broke out to discuss numerous different topics for a few hours after lunch; I ended up participating entirely in the discussion around the [three types of documentation](http://michaeltlombardi.gitlab.io/needful-docs/) and the writing of documentation for both open source projects and internally at our organizations.

Unfortunately, I had to duck out early for my flight home so I missed the takeaways from the other spaces.

#### Takeaways
+ Writing documentation is difficult, usually an afterthought, but should be considered _as important_ as writing tests for code/infrastructure.
+ Documentation should be written iteratively
  + Queing up dozens of pages of docs to write is a sure way to _not_ write them
+ For internal docs, concept docs are hugely important and almost completely overlooked.
  + We usually have people who can figure out _what_ is in place or _how_ to use it but **why** is so often forgotten
+ Documentation for open source projects is one of the primary drivers of adoption
  + But since most maintainers are doing open source because they like writing code, few people write good docs
+ [WriteTheDocs](http://www.writethedocs.org/) is an awesome resource

#### Action Items
+ [x] Work to evangelize/codify best practices for documentation internally and externally
+ [x] Create a project to lower the bar to writing better docs (a skeleton project for documentation)
+ [ ] Work with the maintainers of [Phosphor](https://github.com/PowerShell/Phosphor), [PoshBot](https://github.com/poshbotio/PoshBot), and [PoshSpec](https://github.com/Ticketmaster/poshspec) to build a corpus of _good_ docs people can learn from.
